# env

## ubuntu_server

### 1. pyenv
- [pyenv link](ubuntu_server/pyenv.sh)
- install pyenv, virtualenv, autoenv
- make directory : python2, python3
- versions : python2(2.7.15), python3(3.6.5)
- adapt env : python2, python3
- global env : python3

### 2. install chrome driver : using xvfb
- [install chrome driver](ubuntu_server/chrome_driver_install.sh)
- install Chrome, Selenium, ChromeDriver, Xvfb
- [test code](ubuntu_server/xvfb.py)
